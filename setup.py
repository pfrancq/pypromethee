from setuptools import setup

setup(name='pypromethee',
      version='1.0',
      description='PROMETHEE multi-criteria decision method',
      url='https://gitlab.com/pfrancq/pypromethee.git',
      author='Pascal Francq',
      author_email='pascal@francq.info',
      license='GNU LGPL',
      packages=['pypromethee'],
      zip_safe=False)
